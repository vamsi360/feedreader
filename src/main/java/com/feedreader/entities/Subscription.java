package com.feedreader.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.feedreader.utils.JsonUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * Created by vamsi on 17/03/16.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Subscription {
    private long id;
    private String name;
    private long feedId;
    private long userId;
    private long lastReadArticleId;
    @JsonIgnore
    private Date createdAt;
    @JsonIgnore
    private Date updatedAt;

    public Subscription newSubscription(Subscription subscription) {
        return Subscription.builder()
                .id(subscription.getId())
                .name(subscription.getName())
                .feedId(subscription.getFeedId())
                .userId(subscription.getUserId())
                .lastReadArticleId(subscription.getLastReadArticleId())
                .createdAt(subscription.getCreatedAt())
                .updatedAt(subscription.getUpdatedAt())
                .build();
    }

    public String toJson() throws JsonProcessingException {
        return JsonUtils.getMapper().writeValueAsString(this);
    }
}
