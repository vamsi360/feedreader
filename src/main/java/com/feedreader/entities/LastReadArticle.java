package com.feedreader.entities;

import lombok.Data;

/**
 * Created by vamsi on 18/03/16.
 */
@Data
public class LastReadArticle {
    private int tillArticleId;
}
