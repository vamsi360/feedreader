package com.feedreader.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.feedreader.utils.JsonUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.util.Date;

/**
 * Created by vamsi on 17/03/16.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Article {
    private long id; //auto generated upon publish
    private String articleId; //sent by user
    private Feed feed;
    private String heading;
    private String content;
    private String contentType;
    @JsonIgnore
    private Date createdAt;
    @JsonIgnore
    private Date updatedAt;

    public Article newArticle() {
        return Article.builder()
                .id(id)
                .articleId(articleId)
                .feed(feed)
                .heading(heading)
                .content(content)
                .contentType(contentType)
                .createdAt(createdAt)
                .updatedAt(updatedAt)
                .build();
    }

    public String toJson() throws JsonProcessingException {
        return JsonUtils.getMapper().writeValueAsString(this);
    }

    public static Article fromJson(String json) throws IOException {
        return JsonUtils.getMapper().readValue(json, Article.class);
    }
}
