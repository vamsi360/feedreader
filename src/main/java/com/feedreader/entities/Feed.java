package com.feedreader.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.feedreader.utils.JsonUtils;
import lombok.*;

import java.util.Date;

/**
 * Created by vamsi on 17/03/16.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = {"description", "createdAt", "updatedAt"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Feed {
    private long id;
    private String name;
    private String description;
    @JsonIgnore
    private Date createdAt;
    @JsonIgnore
    private Date updatedAt;

    public String toJson() throws JsonProcessingException {
        return JsonUtils.getMapper().writeValueAsString(this);
    }
}
