package com.feedreader.entities.daos;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

/**
 * Created by vamsi on 19/03/16.
 */
public interface UserDAO {

    @SqlUpdate("insert into users(name, email) values(:name, :email)")
    @GetGeneratedKeys
    long createUser(@Bind("name") String name,
                    @Bind("email") String email);
}
