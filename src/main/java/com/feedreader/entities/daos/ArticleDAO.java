package com.feedreader.entities.daos;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

/**
 * Created by vamsi on 19/03/16.
 */
public interface ArticleDAO {

    @SqlUpdate("insert into articles(article_id, feed_id, heading, content, content_type) " +
            "values(:article_id, :feed_id, :heading, :content, :content_type)")
    @GetGeneratedKeys
    long createArticle(@Bind("article_id") String articleId,
                       @Bind("feed_id") long feedId,
                       @Bind("heading") String heading,
                       @Bind("content") String content,
                       @Bind("content_type") String contentType);

}
