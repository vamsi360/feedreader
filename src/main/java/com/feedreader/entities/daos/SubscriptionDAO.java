package com.feedreader.entities.daos;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

/**
 * Created by vamsi on 19/03/16.
 */
public interface SubscriptionDAO {

    @SqlUpdate("insert into subscriptions(name, feed_id, user_id, last_read_article_id) " +
            "values(:name, :feed_id, :user_id, :last_read_article_id)")
    @GetGeneratedKeys
    long createSubscription(@Bind("name") String name,
                            @Bind("feed_id") long feedId,
                            @Bind("user_id") long userId,
                            @Bind("last_read_article_id") long lastReadArticleId);
}
