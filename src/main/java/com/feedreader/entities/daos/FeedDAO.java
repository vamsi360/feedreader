package com.feedreader.entities.daos;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.GetGeneratedKeys;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

/**
 * Created by vamsi on 19/03/16.
 */
public interface FeedDAO {

    @SqlUpdate("insert into feeds(name, description) values(:name, :description)")
    @GetGeneratedKeys
    long createFeed(@Bind("name") String name,
                    @Bind("description") String description);

}
