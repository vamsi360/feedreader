package com.feedreader;


import com.feedreader.exceptions.*;
import com.feedreader.resources.FeedResource;
import com.feedreader.resources.SubscriptionResource;
import com.feedreader.resources.UserResource;
import com.feedreader.services.*;
import com.feedreader.services.dbimpls.DBArticleService;
import com.feedreader.services.dbimpls.DBFeedService;
import com.feedreader.services.dbimpls.DBSubscriptionService;
import com.feedreader.services.dbimpls.DBUserService;
import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import org.I0Itec.zkclient.ZkClient;
import org.I0Itec.zkclient.ZkConnection;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

/**
 * Main class for FeedReader
 */
public class FeedReaderApplication extends Application<FeedReaderConfiguration> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedReaderApplication.class);

    @Override
    public void initialize(Bootstrap<FeedReaderConfiguration> bootstrap) {
        super.initialize(bootstrap);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void run(FeedReaderConfiguration configuration, Environment environment) throws Exception {
        final DBIFactory dbiFactory = new DBIFactory();
        final DBI jdbi = dbiFactory.build(environment, configuration.getDatabase(), "mysql");

        final UserService userService = new DBUserService(jdbi);

        final FeedService feedService;
        final ArticleService articleService;
        final SubscriptionService subscriptionService;

        final Boolean autoMarkRead = configuration.getAutoMarkRead();
        LOGGER.info("Primary Store: " + configuration.getPrimaryStore() + " with autoMarkRead: " + autoMarkRead);

        if ("mysql".equals(configuration.getPrimaryStore())) {
            feedService = new DBFeedService(jdbi);
            articleService = new DBArticleService(jdbi, feedService);
            subscriptionService = new DBSubscriptionService(jdbi, feedService, articleService, autoMarkRead);
        } else if ("kafka".equals(configuration.getPrimaryStore())) {
            final KafkaConfiguration kafkaConfig = configuration.getKafka();
            final ZkClient zkClient = new ZkClient(
                    kafkaConfig.getZkUrl(),
                    kafkaConfig.getZkSessionTimeoutMs(),
                    kafkaConfig.getZkConnectionTimeoutMs(),
                    ZKStringSerializer$.MODULE$);
            final ZkConnection zkConnection = new ZkConnection(kafkaConfig.getZkUrl(), kafkaConfig.getZkSessionTimeoutMs());
            final ZkUtils zkUtils = new ZkUtils(zkClient, zkConnection, false); //un-secure for now

            final Properties producerProperties = new Properties();
            producerProperties.put("bootstrap.servers", kafkaConfig.getBootstrapServers());
            producerProperties.put("acks", "all");
            producerProperties.put("retries", 0);
            producerProperties.put("key.serializer", kafkaConfig.getKeySerializer());
            producerProperties.put("value.serializer", kafkaConfig.getValueSerializer());

            final Properties consumerProperties = new Properties();
            consumerProperties.put("bootstrap.servers", kafkaConfig.getBootstrapServers());
            //manually managing offsets in DB
            consumerProperties.put("enable.auto.commit", false);
            consumerProperties.put("key.deserializer", kafkaConfig.getKeyDeSerializer());
            consumerProperties.put("value.deserializer", kafkaConfig.getValueDeSerializer());

            feedService = new KafkaFeedService(jdbi, zkUtils, kafkaConfig.getZkUrl());
            final KafkaProducerFactory kafkaProducerFactory = new KafkaProducerFactory(producerProperties);
            articleService = new KafkaArticleService(kafkaProducerFactory);
            subscriptionService = new KafkaSubscriptionService(jdbi, feedService, articleService, consumerProperties, autoMarkRead);
        } else {
            throw new FeedReaderException("Only kafka/mysql are supported");
        }

        final FeedResource feedResource = new FeedResource(
                feedService,
                userService,
                articleService,
                subscriptionService);

        final UserResource userResource = new UserResource(
                userService,
                feedService,
                subscriptionService);

        final SubscriptionResource subscriptionResource = new SubscriptionResource(
                subscriptionService,
                userService,
                feedService);

        environment.jersey().register(feedResource);
        environment.jersey().register(userResource);
        environment.jersey().register(subscriptionResource);

        environment.jersey().register(new AppThrowableMapper());
        environment.jersey().register(new DuplicateEntityExceptionMapper());
        environment.jersey().register(new InvalidEntityExceptionMapper());
        environment.jersey().register(new NonExistentEntityExceptionMapper());
        environment.jersey().register(new FeedReaderExceptionMapper());
    }

    public static void main(String args[]) throws Exception {
        new FeedReaderApplication().run(args);
    }
}
