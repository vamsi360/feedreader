package com.feedreader.utils;

import com.google.common.io.CharStreams;
import com.google.common.io.Files;

import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

/**
 * Created by vamsi on 18/03/16.
 */
public class BaseTestHelper {

    public static String getStringFromFile(ClassLoader classLoader, String relLocation) throws IOException {
        final String fileStr = getFilePath(classLoader, relLocation);

        final File file = new File(fileStr);
        return Files.toString(file, Charset.defaultCharset());
    }

    @SuppressWarnings("ConstantConditions")
    public static String getFilePath(ClassLoader classLoader, String relLocation) {
        return classLoader.getResource(relLocation).getFile();
    }

    public static String getStringEntity(Response response) throws IOException {
        final InputStream inputStream = (InputStream) response.getEntity();
        return CharStreams.toString(new InputStreamReader(inputStream));
    }

}
