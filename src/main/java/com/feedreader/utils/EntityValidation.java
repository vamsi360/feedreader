package com.feedreader.utils;

import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.entities.Subscription;
import com.feedreader.entities.User;
import com.feedreader.exceptions.NonExistentEntityException;
import org.assertj.core.util.Strings;

import java.util.Optional;

/**
 * Created by vamsi on 18/03/16.
 */
public class EntityValidation {

    public static boolean isFeedNameValid(String feedName) {
        int i = 0;
        for (; i < feedName.length(); i++) {
            final char ch = feedName.charAt(i);
            if (!Character.isLetterOrDigit(ch) || ch == '.' || ch == '_') {
                return false;
            }
        }
        return i <= 50;
    }

    public static void checkFeedExistence(long feedId, Optional<Feed> feed) {
        if (!feed.isPresent()) {
            throw new NonExistentEntityException("Feed with id " + feedId + " not found");
        }
    }

    public static void checkFeedExistence(String feedName, Optional<Feed> feed) {
        //LOGGER.debug("Checking if feed with name {} is present", feedName);
        if (!feed.isPresent()) {
            throw new NonExistentEntityException("Feed with name " + feedName + " not found");
        }
    }

    public static void checkSubscriptionExistence(long subscriptionId, Optional<Subscription> subscription) {
        if (!subscription.isPresent()) {
            throw new NonExistentEntityException("Subscription with id " + subscriptionId + " not found");
        }
    }

    public static void checkUserExistence(long userId, Optional<User> user) {
        if (!user.isPresent()) {
            throw new NonExistentEntityException("User with id " + userId + " not found");
        }
    }

    public static boolean isArticleValid(Article article) {
        return !(Strings.isNullOrEmpty(article.getArticleId())
                || article.getContent().length() <= 0
                || article.getHeading().length() <= 0);
    }
}
