package com.feedreader.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.dropwizard.jackson.Jackson;

/**
 * Created by vamsi on 18/03/16.
 */
public class JsonUtils {
    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    public static ObjectMapper getMapper() {
        return MAPPER;
    }

}
