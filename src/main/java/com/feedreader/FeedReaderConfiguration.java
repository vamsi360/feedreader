package com.feedreader;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.client.HttpClientConfiguration;
import io.dropwizard.db.DataSourceFactory;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * Created by vamsi on 17/03/16.
 */
@Data
public class FeedReaderConfiguration extends Configuration {

    @NotEmpty
    private String appName;

    private String primaryStore;

    private Boolean autoMarkRead;

    @NotNull
    private KafkaConfiguration kafka;

    @Valid
    @NotNull
    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();

    @Valid
    private HttpClientConfiguration httpClient = new HttpClientConfiguration();
}
