package com.feedreader;

import lombok.Data;

/**
 * Created by vamsi on 19/03/16.
 */
@Data
public class KafkaConfiguration {
    private String zkUrl;
    private Integer zkSessionTimeoutMs;
    private Integer zkConnectionTimeoutMs;
    private String bootstrapServers;
    private String keySerializer;
    private String valueSerializer;
    private String keyDeSerializer;
    private String valueDeSerializer;
}
