package com.feedreader.resources;

import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.entities.Subscription;
import com.feedreader.entities.User;
import com.feedreader.exceptions.DuplicateEntityException;
import com.feedreader.exceptions.InvalidEntityException;
import com.feedreader.services.ArticleService;
import com.feedreader.services.FeedService;
import com.feedreader.services.SubscriptionService;
import com.feedreader.services.UserService;
import com.feedreader.utils.EntityValidation;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by vamsi on 17/03/16.
 */
@Path("/feeds")
@AllArgsConstructor
public class FeedResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedResource.class);

    private final FeedService feedService;
    private final UserService userService;
    private final ArticleService articleService;
    private final SubscriptionService subscriptionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFeeds(@QueryParam("name") String feedName) {
        final Set<Feed> feeds = Sets.newHashSet();
        if (Strings.isNullOrEmpty(feedName)) {
            feeds.addAll(feedService.getFeeds());
        } else {
            final Optional<Feed> feedOptional = feedService.getFeed(feedName);
            if (feedOptional.isPresent()) {
                feeds.add(feedOptional.get());
            }
        }
        return Response.ok(feeds).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createFeed(Feed feed) throws URISyntaxException {
        LOGGER.info("Request to create feed {}", feed.getName());
        final Optional<Feed> existingFeed = feedService.getFeed(feed.getName());
        if (existingFeed.isPresent()) {
            throw new DuplicateEntityException("Feed with name " + feed.getName() + "is already created");
        }
        if (!EntityValidation.isFeedNameValid(feed.getName())) {
            throw new InvalidEntityException("Feed name " + feed.getName() + " is invalid. It should " +
                    "be <= 50 chars and only alphanumeric or _|. is allowed");
        }

        final long createdFeedId = feedService.createFeed(feed);
        LOGGER.info("Created feed {}", createdFeedId);

        final URI createdURI = new URI("/feeds/" + createdFeedId);
        return Response.created(createdURI).build();
    }

    @GET
    @Path("/{feedId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFeed(@PathParam("feedId") long feedId) {
        final Optional<Feed> feed = feedService.getFeed(feedId);
        EntityValidation.checkFeedExistence(feedId, feed);
        return Response.ok(feed.get()).build();
    }

    @GET
    @Path("/{feedId}/articles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getArticles(@PathParam("feedId") long feedId) {
        final Optional<Feed> feed = feedService.getFeed(feedId);
        EntityValidation.checkFeedExistence(feedId, feed);
        final List<Article> articles = articleService.getArticles(feed.get());
        return Response.ok().entity(articles).build();
    }

    @POST
    @Path("/{feedId}/articles")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response publishArticleToFeed(@PathParam("feedId") long feedId, Article article) {
        final Optional<Feed> feedOptional = feedService.getFeed(feedId);
        EntityValidation.checkFeedExistence(feedId, feedOptional);
        if (!EntityValidation.isArticleValid(article)) {
            throw new InvalidEntityException("Article with articleId " + article.getArticleId() + " published is invalid");
        }

        final Feed feed = feedOptional.get();
        article.setFeed(feed);

        LOGGER.info("Publishing an article to feed with id {} and name {}", feed.getId(), feed.getName());
        final Article publishedArticle = articleService.publishArticleToFeed(article, feed);
        return Response.ok().entity(publishedArticle).build();
    }

    @POST
    @Path("/{feedId}/subscriptions")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response subscribeToFeed(@PathParam("feedId") long feedId,
                                    Subscription subscription) throws URISyntaxException {
        final Optional<Feed> feedOptional = feedService.getFeed(feedId);
        EntityValidation.checkFeedExistence(feedId, feedOptional);
        final Feed feed = feedOptional.get();
        assert subscription.getFeedId() == feed.getId();

        final Optional<User> user = userService.getUser(subscription.getUserId());
        EntityValidation.checkUserExistence(subscription.getUserId(), user);

        LOGGER.info("Subscribing user {} to feed with id {} and name {}", subscription.getUserId(), feedId, feed.getName());
        final long createdSubscriptionId = subscriptionService.createSubscription(subscription);

        final URI uri = new URI("/feeds/" + feedId + "/subscriptions/" + createdSubscriptionId);
        return Response.created(uri).build();
    }

    @GET
    @Path("/{feedId}/subscriptions")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptions(@PathParam("feedId") long feedId) {
        final Optional<Feed> feed = feedService.getFeed(feedId);
        EntityValidation.checkFeedExistence(feedId, feed);

        final Set<Subscription> subscriptions = subscriptionService.getSubscriptions(feed.get());
        return Response.ok(subscriptions).build();
    }
//
//    @GET
//    @Path("/{feed}/subscriptions/{subscriptionId}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response getSubscription(@PathParam("feed") long feed,
//                                    @PathParam("subscriptionId") long subscriptionId) {
//        final Feed feed = feedService.getFeed(feed);
//        final Subscription subscription = subscriptionService.getSubscription(subscriptionId);
//        assert feed.getId() == subscription.getFeed();
//
//        return Response.ok(subscription).build();
//    }
//
//    @GET
//    @Path("/{feed}/subscriptions/{subscriptionId}/articles")
//    @Consumes(MediaType.APPLICATION_JSON)
//    public Response getArticlesForSubscription(@PathParam("feed") long feed,
//                                               @PathParam("subscriptionId") long subscriptionId,
//                                               @QueryParam("count") @DefaultValue("10") int count) {
//        final Feed feed = feedService.getFeed(feed);
//        final Subscription subscription = subscriptionService.getSubscription(subscriptionId);
//        assert feed.getId() == subscription.getFeed();
//
//        final List<Article> articles = subscriptionService.getArticles(subscription, count);
//        return Response.ok(articles).build();
//    }
}
