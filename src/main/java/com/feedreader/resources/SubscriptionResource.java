package com.feedreader.resources;

import com.feedreader.entities.*;
import com.feedreader.exceptions.DuplicateEntityException;
import com.feedreader.services.FeedService;
import com.feedreader.services.SubscriptionService;
import com.feedreader.services.UserService;
import com.feedreader.utils.EntityValidation;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by vamsi on 18/03/16.
 */
@AllArgsConstructor
@Path("/subscriptions")
public class SubscriptionResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionResource.class);

    private final SubscriptionService subscriptionService;
    private final UserService userService;
    private final FeedService feedService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptions(@QueryParam("userId") @DefaultValue("0") long userId,
                                     @QueryParam("feedId") @DefaultValue("0") long feedId) {
        final Set<Subscription> subscriptions = Sets.newHashSet();

        if (userId != 0 && feedId != 0) {
            final Optional<Feed> feed = feedService.getFeed(feedId);
            EntityValidation.checkFeedExistence(feedId, feed);
            final Optional<User> user = userService.getUser(userId);
            EntityValidation.checkUserExistence(userId, user);
            final Optional<Subscription> subscription = subscriptionService.getSubscription(feed.get(), user.get());
            subscriptions.add(subscription.get());
        } else if (userId != 0) {
            final Optional<User> user = userService.getUser(userId);
            EntityValidation.checkUserExistence(userId, user);
            subscriptions.addAll(subscriptionService.getSubscriptions(user.get()));
        } else if (feedId != 0) {
            final Optional<Feed> feed = feedService.getFeed(feedId);
            EntityValidation.checkFeedExistence(feedId, feed);
            subscriptions.addAll(subscriptionService.getSubscriptions(feed.get()));
        } else {
            subscriptions.addAll(subscriptionService.getSubscriptions());
        }

        return Response.ok().entity(subscriptions).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createSubscription(Subscription subscription) throws URISyntaxException {
        final Optional<Feed> feed = feedService.getFeed(subscription.getFeedId());
        EntityValidation.checkFeedExistence(subscription.getFeedId(), feed);

        final Optional<User> user = userService.getUser(subscription.getUserId());
        EntityValidation.checkUserExistence(subscription.getUserId(), user);

        final Optional<Subscription> subscriptionOptional = subscriptionService.getSubscription(feed.get(), user.get());
        if (subscriptionOptional.isPresent()) {
            throw new DuplicateEntityException("subscription with feedId " + subscription.getFeedId() +
                    " and userId " + subscription.getUserId() + " already exists");
        }
        final long createdSubscriptionId = subscriptionService.createSubscription(subscription);
        final URI uri = new URI("/subscriptions/" + createdSubscriptionId);

        return Response.created(uri).build();
    }

    @DELETE
    @Path("/{subscriptionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSubscription(@PathParam("subscriptionId") long subscriptionId) throws URISyntaxException {
        final Optional<Subscription> subscription = subscriptionService.getSubscription(subscriptionId);
        EntityValidation.checkSubscriptionExistence(subscriptionId, subscription);

        subscriptionService.deleteSubscription(subscription.get());
        return Response.ok().build();
    }

    @GET
    @Path("/{subscriptionId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscription(@PathParam("subscriptionId") long subscriptionId) {
        final Optional<Subscription> subscription = subscriptionService.getSubscription(subscriptionId);
        EntityValidation.checkSubscriptionExistence(subscriptionId, subscription);

        return Response.ok(subscription.get()).build();
    }

    /**
     * api to browse through the articles and get:
     * un-read articles (and)
     * history articles if given an index to query from
     *
     * @param subscriptionId to get articles for
     * @param startArticleId of the articles for the feed
     * @param count          the no of articles to get
     * @return the response with the articles
     */
    @GET
    @Path("/{subscriptionId}/articles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getArticlesForSubscription(@PathParam("subscriptionId") long subscriptionId,
                                               @QueryParam("startArticleId") @DefaultValue("0") long startArticleId,
                                               @QueryParam("count") @DefaultValue("10") int count) {
        final Optional<Subscription> subscription = subscriptionService.getSubscription(subscriptionId);
        EntityValidation.checkSubscriptionExistence(subscriptionId, subscription);

        final List<Article> articles = Lists.newLinkedList();
        if (startArticleId == 0) {
            articles.addAll(subscriptionService.getArticles(subscription.get(), count));
        } else {
            articles.addAll(subscriptionService.getArticles(subscription.get(), startArticleId, count));
        }

        return Response.ok(articles).build();
    }

    @PUT
    @Path("/{subscriptionId}/actions/mark-read")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response markRead(@PathParam("subscriptionId") long subscriptionId,
                             LastReadArticle lastReadArticle) {
        final Optional<Subscription> subscription = subscriptionService.getSubscription(subscriptionId);
        EntityValidation.checkSubscriptionExistence(subscriptionId, subscription);

        subscriptionService.markReadTill(subscription.get(), lastReadArticle.getTillArticleId());
        return Response.noContent().build();
    }

}
