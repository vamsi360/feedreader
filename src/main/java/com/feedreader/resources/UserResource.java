package com.feedreader.resources;

import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.entities.Subscription;
import com.feedreader.entities.User;
import com.feedreader.services.FeedService;
import com.feedreader.services.SubscriptionService;
import com.feedreader.services.UserService;
import com.feedreader.utils.EntityValidation;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import org.assertj.core.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Created by vamsi on 17/03/16.
 */
@AllArgsConstructor
@Path("/users")
public class UserResource {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserResource.class);

    private final UserService userService;
    private final FeedService feedService;
    private final SubscriptionService subscriptionService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers() {
        final Set<User> users = userService.getUsers();
        return Response.ok(users).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createUser(User user) throws URISyntaxException {
        final long createdUserId = userService.createUser(user);
        final URI url = new URI("/users/" + createdUserId);
        return Response.created(url).build();
    }

    @GET
    @Path("/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@PathParam("userId") long userId) {
        final Optional<User> user = userService.getUser(userId);
        EntityValidation.checkUserExistence(userId, user);

        return Response.ok(user.get()).build();
    }

    @GET
    @Path("/{userId}/subscriptions")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSubscriptions(@PathParam("userId") long userId) {
        final Optional<User> user = userService.getUser(userId);
        EntityValidation.checkUserExistence(userId, user);

        final Set<Subscription> subscriptions = subscriptionService.getSubscriptions(user.get());
        return Response.ok(subscriptions).build();
    }

    /**
     * Bulk api to get all the feed articles a user subscribed to
     * You can query for specific feeds by giving in a csv format ?feedId=1,2
     * The maxNoOfArticles per feed can be given by ?count=12. Default value is 10
     * <p>
     * Sample usage:
     * /users/{userId}/articles?count=20
     * /users/{userId}/articles?count=10&feedId=1,2
     *
     * @param userId  to get articles for
     * @param uriInfo that has queryParams
     * @return the response with articles selected
     */
    @GET
    @Path("/{userId}/articles")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getArticles(@PathParam("userId") long userId,
                                @Context UriInfo uriInfo) {
        final Optional<User> userOptional = userService.getUser(userId);
        EntityValidation.checkUserExistence(userId, userOptional);
        final User user = userOptional.get();

        final MultivaluedMap<String, String> queryParameters = uriInfo.getQueryParameters();
        final Set<Subscription> subscriptions = Sets.newHashSet();

        int noOfArticles = SubscriptionService.DEFAULT_NO_OF_ARTICLES_TO_GET;
        final List<Long> feedIds = getFeedIds(queryParameters, "feedId");
        String countStr = queryParameters.getFirst("count");
        if (!Strings.isNullOrEmpty(countStr)) {
            try {
                noOfArticles = Integer.valueOf(countStr);
            } catch (NumberFormatException ignored) {
            }
        }

        LOGGER.info("FeedIds requested: " + feedIds);
        if (feedIds.size() == 0) {
            subscriptions.addAll(subscriptionService.getSubscriptions(user));
        } else {
            feedIds.forEach((Long feedId) -> {
                Optional<Feed> feed = feedService.getFeed(feedId);
                EntityValidation.checkFeedExistence(feedId, feed);
                final Optional<Subscription> subscription = subscriptionService.getSubscription(feed.get(), user);
                if (subscription.isPresent()) {
                    subscriptions.add(subscription.get());
                }
            });
        }

        final Map<Subscription, List<Article>> subscriptionArticles =
                subscriptionService.getArticles(subscriptions, noOfArticles);

        //conversion to make key a simpler feedId
        final Map<Long, List<Article>> feedArticlesMap = Maps.newHashMap();
        final Set<Map.Entry<Subscription, List<Article>>> entries = subscriptionArticles.entrySet();
        entries.forEach((Map.Entry<Subscription, List<Article>> entry) -> {
            final Subscription subscription = entry.getKey();
            feedArticlesMap.put(subscription.getFeedId(), entry.getValue());
        });

        return Response.ok(feedArticlesMap).build();
    }

    public List<Long> getFeedIds(MultivaluedMap<String, String> queryParameters, String key) {
        final List<Long> values = Lists.newArrayList();
        final String csValues = queryParameters.getFirst(key);
        if (csValues == null) {
            return values;
        }
        final String[] splitValues = csValues.split(",");
        for (String splitValue : splitValues) {
            values.add(Long.parseLong(splitValue.trim()));
        }
        return values;
    }

}
