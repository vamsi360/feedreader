package com.feedreader.services;

import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;

import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by vamsi on 18/03/16.
 */
public interface FeedService {

    /**
     * creates a new feed
     *
     * @param feed to create
     */
    long createFeed(Feed feed);

    /**
     * get the unique feed given an id
     *
     * @param id to get feed for
     * @return the Optional feed - it can be a valid feed or empty
     */
    Optional<Feed> getFeed(long id);

    /**
     * get the unique feed given a name
     *
     * @param name to get feed for
     * @return the Optional feed - it can be a valid feed or empty
     */
    Optional<Feed> getFeed(String name);

    /**
     * get all the feeds
     *
     * @return all the registered feeds
     */
    Set<Feed> getFeeds();

    /**
     * get the articles for a given feed
     * mostly req. by the author but not supported mostly without a subscription by some implementations
     *
     * @param feed to get articles for
     * @return the articles
     */
    List<Article> getArticles(Feed feed);
}
