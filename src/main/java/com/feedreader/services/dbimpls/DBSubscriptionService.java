package com.feedreader.services.dbimpls;

import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.entities.Subscription;
import com.feedreader.exceptions.NonExistentEntityException;
import com.feedreader.services.ArticleService;
import com.feedreader.services.FeedService;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

/**
 * Created by vamsi on 17/03/16.
 */
public class DBSubscriptionService extends DefaultSubscriptionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DBSubscriptionService.class);

    private final boolean autoMarkRead;

    public DBSubscriptionService(DBI dbi,
                                 FeedService feedService,
                                 ArticleService articleService,
                                 boolean autoMarkRead) {
        super(dbi, feedService, articleService);
        this.autoMarkRead = autoMarkRead;
    }

    @Override
    public List<Article> getArticles(Subscription subscription, long startArticleId, int count) {
        final Optional<Feed> feed = feedService.getFeed(subscription.getFeedId());
        if (!feed.isPresent()) {
            throw new NonExistentEntityException("Feed with id " + subscription.getFeedId() + " not found");
        }
        final List<Article> articles = articleService.getArticles(feed.get(), startArticleId, count);
        final long lastArticleId = articles.get(articles.size() - 1).getId();
        if (autoMarkRead) {
            markReadTill(subscription, lastArticleId + 1);
        }

        return articles;
    }
}
