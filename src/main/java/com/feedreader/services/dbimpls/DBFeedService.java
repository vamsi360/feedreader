package com.feedreader.services.dbimpls;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.entities.daos.FeedDAO;
import com.feedreader.services.FeedService;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.NotImplementedException;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by vamsi on 17/03/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
public class DBFeedService implements FeedService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DBFeedService.class);
    private final DBI dbi;

    @Override
    public long createFeed(Feed feed) {
        LOGGER.info("Creating feed {}", feed); //has toString()
        final FeedDAO dao = dbi.open(FeedDAO.class);
        try {
            return dao.createFeed(feed.getName(), feed.getDescription());
        } finally {
            dbi.close(dao);
        }
    }

    @Override
    public Optional<Feed> getFeed(long id) {
        LOGGER.info("Getting feed for id {}", id);
        try (Handle handle = dbi.open()) {
            return Optional.ofNullable(
                    handle.createQuery("select * from feeds where id = :id")
                            .bind("id", id)
                            .map((index, r, ctx) -> {
                                return getFeed(r);
                            })
                            .first());
        }
    }

    @Override
    public Optional<Feed> getFeed(String name) {
        LOGGER.info("Getting feed for name {}", name);
        try (Handle handle = dbi.open()) {
            return Optional.ofNullable(
                    handle.createQuery("select * from feeds where name = :name")
                            .bind("name", name)
                            .map((index, r, ctx) -> {
                                return getFeed(r);
                            })
                            .first());
        }
    }

    private Feed getFeed(ResultSet r) throws SQLException {
        return Feed.builder()
                .id(r.getLong("id"))
                .name(r.getString("name"))
                .description(r.getString("description"))
                .createdAt(r.getDate("created_at"))
                .updatedAt(r.getDate("updated_at"))
                .build();
    }

    @Override
    public Set<Feed> getFeeds() {
        LOGGER.info("Getting all the feeds");
        try (Handle handle = dbi.open()) {
            return Sets.newHashSet(
                    handle.createQuery("select * from feeds")
                            .map((index, r, ctx) -> {
                                return getFeed(r);
                            })
                            .list());
        }
    }

    @Override
    public List<Article> getArticles(Feed feed) {
        throw new NotImplementedException("Viewing directly the feed without subscription is not supported currently");
    }
}
