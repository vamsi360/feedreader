package com.feedreader.services.dbimpls;

import com.feedreader.entities.User;
import com.feedreader.entities.daos.UserDAO;
import com.feedreader.services.UserService;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Set;

/**
 * Created by vamsi on 17/03/16.
 */
@AllArgsConstructor
public class DBUserService implements UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DBUserService.class);
    private final DBI dbi;

    @Override
    public long createUser(User user) {
        LOGGER.info("Creating user {}", user);
        final UserDAO dao = dbi.open(UserDAO.class);
        try {
            return dao.createUser(user.getName(), user.getEmail());
        } finally {
            dbi.close(dao);
        }
    }

    @Override
    public Optional<User> getUser(long id) {
        LOGGER.info("Getting user for id {}", id);
        try (Handle handle = dbi.open()) {
            return Optional.ofNullable(handle.createQuery("select * from users where id = :id")
                    .bind("id", id)
                    .map((index, r, ctx) -> {
                        return getUser(r);
                    })
                    .first());
        }
    }

    private User getUser(ResultSet r) throws SQLException {
        return User.builder()
                .id(r.getLong("id"))
                .name(r.getString("name"))
                .email(r.getString("email"))
                .createdAt(r.getDate("created_at"))
                .updatedAt(r.getDate("updated_at"))
                .build();
    }

    @Override
    public Set<User> getUsers() {
        LOGGER.info("Getting all the users");
        try (Handle handle = dbi.open()) {
            return Sets.newHashSet(handle.createQuery("select * from users")
                    .map((index, r, ctx) -> {
                        return getUser(r);
                    })
                    .list());
        }
    }

}
