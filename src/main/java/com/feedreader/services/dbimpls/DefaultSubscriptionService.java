package com.feedreader.services.dbimpls;

import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.entities.Subscription;
import com.feedreader.entities.User;
import com.feedreader.entities.daos.SubscriptionDAO;
import com.feedreader.services.ArticleService;
import com.feedreader.services.FeedService;
import com.feedreader.services.SubscriptionService;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Created by vamsi on 19/03/16.
 */
@AllArgsConstructor
public abstract class DefaultSubscriptionService implements SubscriptionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DBSubscriptionService.class);

    protected final DBI dbi;
    protected final FeedService feedService;
    protected final ArticleService articleService;

    @Override
    public long createSubscription(final Subscription subscription) {
        LOGGER.info("Creating subscription {}", subscription);
        final SubscriptionDAO dao = dbi.open(SubscriptionDAO.class);
        try {
            return dao.createSubscription(
                    subscription.getName(),
                    subscription.getFeedId(),
                    subscription.getUserId(),
                    subscription.getLastReadArticleId());
        } finally {
            dbi.close(dao);
        }
    }

    @Override
    public void updateSubscription(final Subscription subscription) {
        LOGGER.info("Updating subscription to {}", subscription);
        try (Handle handle = dbi.open()) {
            handle.update("update subscriptions set " +
                            "name = ?, feed_id = ?, user_id = ?, last_read_article_id = ? where id = ?",
                    subscription.getName(),
                    subscription.getFeedId(),
                    subscription.getUserId(),
                    subscription.getLastReadArticleId(),
                    subscription.getId());
        }
    }

    @Override
    public void markReadTill(final Subscription subscription, final long articleId) {
        LOGGER.info("Marking subscription id {} with name {} as read till articleId {}",
                subscription.getId(), subscription.getName(), articleId);

        final Subscription newSubscription = subscription.newSubscription(subscription);
        newSubscription.setLastReadArticleId(articleId);
        updateSubscription(newSubscription);
    }

    @Override
    public Set<Subscription> getSubscriptions() {
        LOGGER.info("Getting all the subscriptions");
        try (Handle handle = dbi.open()) {
            return Sets.newHashSet(handle.createQuery("select * from subscriptions")
                    .map((index, r, ctx) -> {
                        return getSubscription(r);
                    })
                    .list());
        }
    }

    @Override
    public Set<Subscription> getSubscriptions(Feed feed) {
        try (Handle handle = dbi.open()) {
            return Sets.newHashSet(handle.createQuery("select * from subscriptions where feed_id = :feed_id")
                    .bind("feed_id", feed.getId())
                    .map((index, r, ctx) -> {
                        return getSubscription(r);
                    })
                    .list());
        }
    }

    @Override
    public Set<Subscription> getSubscriptions(User user) {
        LOGGER.info("Getting the subscriptions for user {} with name {}", user.getId(), user.getName());
        try (Handle handle = dbi.open()) {
            return Sets.newHashSet(handle.createQuery("select * from subscriptions where user_id = :user_id")
                    .bind("user_id", user.getId())
                    .map((index, r, ctx) -> {
                        return getSubscription(r);
                    })
                    .list());
        }
    }

    @Override
    public Optional<Subscription> getSubscription(long id) {
        LOGGER.info("Getting the subscription with id {}", id);
        try (Handle handle = dbi.open()) {
            return Optional.ofNullable(handle.createQuery("select * from subscriptions where id = :id")
                    .bind("id", id)
                    .map((index, r, ctx) -> {
                        return getSubscription(r);
                    })
                    .first());
        }
    }

    private Subscription getSubscription(ResultSet r) throws SQLException {
        return Subscription.builder()
                .id(r.getLong("id"))
                .name(r.getString("name"))
                .feedId(r.getLong("feed_id"))
                .userId(r.getLong("user_id"))
                .lastReadArticleId(r.getLong("last_read_article_id"))
                .createdAt(r.getDate("created_at"))
                .updatedAt(r.getDate("updated_at"))
                .build();
    }

    @Override
    public Optional<Subscription> getSubscription(Feed feed, User user) {
        LOGGER.info("Getting the subscription for feed {} and user {}", feed.getId(), user.getId());
        try (Handle handle = dbi.open()) {
            return Optional.ofNullable(handle.createQuery("select * from subscriptions where feed_id = :feed_id and user_id = :user_id")
                    .bind("feed_id", feed.getId())
                    .bind("user_id", user.getId())
                    .map((index, r, ctx) -> {
                        return getSubscription(r);
                    })
                    .first());
        }
    }

    @Override
    public List<Article> getArticles(Subscription subscription) {
        return getArticles(subscription, SubscriptionService.DEFAULT_NO_OF_ARTICLES_TO_GET);
    }

    public abstract List<Article> getArticles(Subscription subscription, long startArticleId, int count);

    @Override
    public List<Article> getArticles(Subscription subscription, int count) {
        return getArticles(subscription, subscription.getLastReadArticleId(), count);
    }

    @Override
    public Map<Subscription, List<Article>> getArticles(Set<Subscription> subscriptions, int maxCountPerSubscription) {
        final Map<Subscription, List<Article>> subArticlesMap = Maps.newHashMap();
        subscriptions.forEach(subscription -> {
            LOGGER.info("Getting {} of articles max. for subscription {}", maxCountPerSubscription, subscription.getId());
            final long startArticleId = subscription.getLastReadArticleId();
            final List<Article> subArticles = getArticles(subscription, startArticleId, maxCountPerSubscription);
            subArticlesMap.put(subscription, subArticles);
        });
        return subArticlesMap;
    }

    @Override
    public void deleteSubscription(Subscription subscription) {
        LOGGER.info("Deleting subscription {}", subscription); //using toString()
        try (Handle handle = dbi.open()) {
            handle.update("delete from subscriptions where id = ?", subscription.getId());
        }
    }
}
