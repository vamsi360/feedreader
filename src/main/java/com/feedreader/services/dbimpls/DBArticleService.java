package com.feedreader.services.dbimpls;

import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.entities.daos.ArticleDAO;
import com.feedreader.services.ArticleService;
import com.feedreader.services.FeedService;
import com.feedreader.utils.EntityValidation;
import lombok.AllArgsConstructor;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Created by vamsi on 18/03/16.
 */
@AllArgsConstructor
public class DBArticleService implements ArticleService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DBArticleService.class);

    private final DBI dbi;
    private final FeedService feedService;

    @Override
    public Article publishArticleToFeed(Article article, Feed feed) {
        LOGGER.debug("Publishing article {} for feed with id {} and name {}", article, feed.getId(), feed.getName());
        final ArticleDAO dao = dbi.open(ArticleDAO.class);
        try {
            final long id = dao.createArticle(article.getArticleId(),
                    article.getFeed().getId(),
                    article.getHeading(),
                    article.getContent(),
                    article.getContentType());
            final Article newArticle = article.newArticle();
            newArticle.setId(id);

            return newArticle;
        } finally {
            dbi.close(dao);
        }
    }

    @Override
    public Optional<Article> getArticle(long id) {
        LOGGER.info("Getting the article for id {}", id);
        try (Handle handle = dbi.open()) {
            return Optional.ofNullable(handle.createQuery("select * from articles where id = :id")
                    .bind("id", id)
                    .map((index, r, ctx) -> {
                        return getArticle(r);
                    })
                    .first());
        }
    }

    @Override
    public List<Article> getArticles(Feed feed) {
        LOGGER.info("Getting articles for feed with id {} and name {}", feed.getId(), feed.getName());
        try (Handle handle = dbi.open()) {
            return handle.createQuery("select * from articles where feed_id = :feed_id")
                    .bind("feed_id", feed.getId())
                    .map((index, r, ctx) -> {
                        return getArticle(r);
                    })
                    .list();
        }
    }

    private Article getArticle(ResultSet r) throws SQLException {
        final long feedId = r.getLong("feed_id");
        final Optional<Feed> feed = feedService.getFeed(feedId);
        EntityValidation.checkFeedExistence(feedId, feed);

        return Article.builder()
                .id(r.getLong("id"))
                .articleId(r.getString("article_id"))
                .feed(feed.get())
                .heading(r.getString("heading"))
                .content(r.getString("content"))
                .contentType(r.getString("content_type"))
                .createdAt(r.getDate("created_at"))
                .updatedAt(r.getDate("updated_at"))
                .build();
    }

    @Override
    public List<Article> getArticles() {
        LOGGER.info("Getting all the articles");
        try (Handle handle = dbi.open()) {
            return handle.createQuery("select * from articles")
                    .map((index, r, ctx) -> {
                        return getArticle(r);
                    })
                    .list();
        }
    }

    @Override
    public List<Article> getArticles(Feed feed, long startArticleId, int noOfArticles) {
        LOGGER.debug("Getting {} of articles max. starting with article id {} for feed with id {} and name {}",
                noOfArticles, startArticleId, feed.getId(), feed.getName());
        try (Handle handle = dbi.open()) {
            return handle.createQuery("select * from articles " +
                    "where feed_id = :feed_id and id >= :id")
                    .bind("feed_id", feed.getId())
                    .bind("id", startArticleId)
                    .map((index, r, ctx) -> {
                        return getArticle(r);
                    })
                    .list(noOfArticles);
        }
    }
}
