package com.feedreader.services;

import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.entities.Subscription;
import com.feedreader.entities.User;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Created by vamsi on 18/03/16.
 */
public interface SubscriptionService {
    int DEFAULT_NO_OF_ARTICLES_TO_GET = 10;
    int MAX_NO_OF_ARTICLES_TO_GET = 100;

    /**
     * create a subscription
     *
     * @param subscription to create
     */
    long createSubscription(final Subscription subscription);

    /**
     * update the given subscription in store
     *
     * @param subscription to update in updated form
     */
    void updateSubscription(final Subscription subscription);

    /**
     * mark the articles as read till the articleId for a given subscription
     *
     * @param subscription to mark read till for
     * @param articleId    till when the subscription is read
     */
    void markReadTill(final Subscription subscription, final long articleId);

    /**
     * get all the subscriptions registered
     *
     * @return the subscriptions
     */
    Set<Subscription> getSubscriptions();

    /**
     * get all the subscriptions for a given feed
     *
     * @param feed to get subscriptions for
     * @return the subscriptions for the feed
     */
    Set<Subscription> getSubscriptions(Feed feed);

    /**
     * get the subscriptions for a given user
     *
     * @param user to get subscriptions for
     * @return the subscribed feed subscriptions
     */
    Set<Subscription> getSubscriptions(User user);

    /**
     * get the subscription given a id. always one entry per id
     *
     * @param subscriptionId to get subscription for
     * @return the subscription
     */
    Optional<Subscription> getSubscription(long subscriptionId);

    /**
     * get the subscription for a feed and a user which uniquely identify a subscription
     *
     * @param feed to get subscription for
     * @param user to get subscription for
     * @return the subscription
     */
    Optional<Subscription> getSubscription(Feed feed, User user);

    /**
     * get all the articles for a given subscription
     *
     * @param subscription to get articles for
     * @return the articles
     */
    List<Article> getArticles(Subscription subscription);

    /**
     * get the articles for a given subscription starting with the startArticleId
     * and till count no of messages are read
     *
     * @param subscription   to get articles for
     * @param startArticleId the articleId to start from
     * @param count          the no of articles to get at max.
     * @return the articles selected
     */
    List<Article> getArticles(Subscription subscription, long startArticleId, int count);

    /**
     * get at max. @param count no of articles for a given subscription
     *
     * @param subscription to get articles for in sequence
     * @param count        the number of articles to get
     * @return the articles
     */
    List<Article> getArticles(Subscription subscription, int count);

    /**
     * get the articles for the given set of subscriptions
     *
     * @param subscriptions           to get articles for
     * @param maxCountPerSubscription the max no of articles per subscription
     * @return the articles
     */
    Map<Subscription, List<Article>> getArticles(Set<Subscription> subscriptions, int maxCountPerSubscription);

    /**
     * delete the given subscription
     *
     * @param subscription to delete
     */
    void deleteSubscription(Subscription subscription);
}
