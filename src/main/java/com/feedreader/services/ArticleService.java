package com.feedreader.services;

import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;

import java.util.List;
import java.util.Optional;

/**
 * Created by vamsi on 18/03/16.
 */
public interface ArticleService {
    /**
     * publish the article to a feed. Used by the author to publish
     *  @param article to publish
     * @param feed    to publish article to
     */
    Article publishArticleToFeed(Article article, Feed feed);

    /**
     * @param id to get article for
     * @return the article given id
     */
    Optional<Article> getArticle(long id);

    /**
     * get all the articles for the given feed
     *
     * @param feed to get articles for
     * @return the list of articles
     */
    List<Article> getArticles(Feed feed);

    /**
     * get all articles. *Scary method*
     *
     * @return the articles
     */
    List<Article> getArticles();


    /**
     * get the articles for a given feed starting from a startId
     *
     * @param feed         to get articles for
     * @param startId      articleId to start from
     * @param noOfArticles to get at max. from the feed
     * @return the articles
     */
    List<Article> getArticles(Feed feed, long startId, int noOfArticles);
}
