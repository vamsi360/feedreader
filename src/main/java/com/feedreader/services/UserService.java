package com.feedreader.services;

import com.feedreader.entities.User;

import java.util.Optional;
import java.util.Set;

/**
 * Created by vamsi on 18/03/16.
 */
public interface UserService {

    /**
     * create a new user
     *
     * @param user to create
     */
    long createUser(User user);

    /**
     * get the unique user by id
     *
     * @param id to get user for
     * @return the user for the id
     */
    Optional<User> getUser(long id);

    /**
     * get all the registered users
     *
     * @return the registered users
     */
    Set<User> getUsers();
}
