package com.feedreader.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.exceptions.ArticlePublishingException;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Optional;

/**
 * Created by vamsi on 19/03/16.
 */
@AllArgsConstructor
public class KafkaArticleService implements ArticleService {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaArticleService.class);

    private final KafkaProducerFactory producerFactory;

    @Override
    public Article publishArticleToFeed(final Article article, final Feed feed) {
        try {
            final KafkaProducer<String, String> producer = producerFactory.getProducer(feed);
            final ProducerRecord<String, String> record = new ProducerRecord<>(feed.getName(), article.toJson());
            final RecordMetadata recordMetadata = producer.send(record).get();
            LOGGER.debug("Written article {} to Kafka topic {} and partition {}",
                    article.getArticleId(), recordMetadata.topic(), recordMetadata.partition());

            final Article newArticle = article.newArticle();
            newArticle.setId(recordMetadata.offset());

            return newArticle;
        } catch (JsonProcessingException ex) {
            throw new ArticlePublishingException("Unable to serialize the article to Json");
        } catch (Exception ex) {
            final String msg = "Exception in sending article record to Kafka: " + ex.getMessage();
            LOGGER.error(msg, ex);
            throw new ArticlePublishingException(msg);
        }
    }

    @Override
    public Optional<Article> getArticle(long id) {
        //TODO: not always the case - using a consumer where offsets can be set, this can be changed
        throw new NotImplementedException("Getting an article by id is not supported as it can be inefficient in a non-indexed log");
    }

    @Override
    public List<Article> getArticles(Feed feed) {
        throw new NotImplementedException("");
    }

    @Override
    public List<Article> getArticles() {
        throw new NotImplementedException("");
    }

    @Override
    public List<Article> getArticles(Feed feed, long startId, int noOfArticles) {
        throw new NotImplementedException("");
    }
}
