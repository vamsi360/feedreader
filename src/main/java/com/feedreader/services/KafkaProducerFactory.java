package com.feedreader.services;

import com.feedreader.entities.Feed;
import lombok.AllArgsConstructor;
import org.apache.kafka.clients.producer.KafkaProducer;

import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by vamsi on 19/03/16.
 */
@AllArgsConstructor
public class KafkaProducerFactory {

    private final Properties producerProperties;

    //not using Map interface as it *has to be* concurrent
    private final ConcurrentHashMap<Feed, KafkaProducer<String, String>> producerMap = new ConcurrentHashMap<>();

    public KafkaProducer<String, String> getProducer(Feed feed) {
        if (!producerMap.containsKey(feed)) {
            producerMap.putIfAbsent(feed, new KafkaProducer<>(producerProperties));
        }
        return producerMap.get(feed);
    }
}
