package com.feedreader.services;

import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.exceptions.NonExistentEntityException;
import com.feedreader.services.dbimpls.DBFeedService;
import com.google.common.collect.Sets;
import kafka.admin.AdminUtils;
import kafka.utils.ZkUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.collection.Seq;
import scala.collection.convert.WrapAsJava$;

import java.util.*;

/**
 * Created by vamsi on 19/03/16.
 */
public class KafkaFeedService extends DBFeedService {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaFeedService.class);

    public static final int PARTITIONS = 1;
    public static final int REPLICATION_FACTOR = 1;

    private final ZkUtils zkUtils;
    private final String zkUrl;

    public KafkaFeedService(DBI dbi, ZkUtils zkUtils, String zkUrl) {
        super(dbi);
        this.zkUtils = zkUtils;
        this.zkUrl = zkUrl;
    }

    @Override
    public long createFeed(Feed feed) {
        AdminUtils.createTopic(zkUtils, feed.getName(), PARTITIONS, REPLICATION_FACTOR, new Properties());
        return super.createFeed(feed);
    }

    @Override
    public Optional<Feed> getFeed(long id) {
        return super.getFeed(id);
    }

    @Override
    public Optional<Feed> getFeed(String name) {
        final Seq<String> allTopics = zkUtils.getAllTopics();
        if (allTopics.contains(name)) {
            throw new NonExistentEntityException("Feed Topic with name " + name + " doesn't exist in Kafka");
        }
        return super.getFeed(name);
    }

    @Override
    public Set<Feed> getFeeds() {
        final Set<String> topicNames = Sets.newHashSet(WrapAsJava$.MODULE$.seqAsJavaList(zkUtils.getAllTopics()));
        LOGGER.debug("Topics in Kafka {}", Collections.singletonList(topicNames));
        final Set<Feed> dbFeeds = super.getFeeds();
        dbFeeds.forEach((Feed feed) -> {
            if (!topicNames.contains(feed.getName())) {
                throw new NonExistentEntityException("Feed with id " +
                        feed.getId() + " and name " + feed.getName() + "  doesn't have a topic in Kafka");
            }
        });
        return dbFeeds;
    }

    @Override
    public List<Article> getArticles(Feed feed) {
        throw new NotImplementedException("Not supported to view all articles from a kafka topic. Use subscriptionService instead");
    }
}
