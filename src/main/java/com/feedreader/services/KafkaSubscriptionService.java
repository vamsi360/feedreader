package com.feedreader.services;

import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.entities.Subscription;
import com.feedreader.exceptions.FeedReaderException;
import com.feedreader.services.dbimpls.DefaultSubscriptionService;
import com.feedreader.utils.EntityValidation;
import com.google.common.collect.Lists;
import org.apache.kafka.clients.consumer.ConsumerRebalanceListener;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by vamsi on 19/03/16.
 */
public class KafkaSubscriptionService extends DefaultSubscriptionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaSubscriptionService.class);

    private final Properties consumerProperties;
    private final boolean autoMarkRead;

    public static final long CONSUMER_FETCH_RECORDS_MAX_TIME_MS = 2000;
    //purposefully limiting poll time to 500ms
    public static final long CONSUMER_POLL_MAX_TIME_MS = 500;

    public KafkaSubscriptionService(DBI dbi,
                                    FeedService feedService,
                                    ArticleService articleService,
                                    Properties consumerProperties,
                                    boolean autoMarkRead) {
        super(dbi, feedService, articleService);
        this.consumerProperties = consumerProperties;
        this.autoMarkRead = autoMarkRead;
    }

    @Override
    public long createSubscription(Subscription subscription) {
        return super.createSubscription(subscription);
    }

    @Override
    public List<Article> getArticles(Subscription subscription, long startArticleId, int count) {
        final Feed feed = getFeed(subscription);
        consumerProperties.put("group.id", "subscription-" + subscription.getId());

        try (KafkaConsumer<String, String> consumer = new KafkaConsumer<>(consumerProperties)) {
            consumer.subscribe(Collections.singletonList(feed.getName()), new ConsumerRebalanceListener() {
                @Override
                public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
                    LOGGER.debug("Partitions are revoked for subscriber {} to feed {}", subscription.getId(), feed.getName());
                }

                @Override
                public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
                    LOGGER.debug("Partitions are assigned for subscriber {} to feed {}", subscription.getId(), feed.getName());
                    final TopicPartition topicPartition = new TopicPartition(feed.getName(), 0);
                    if (subscription.getLastReadArticleId() == 0) {
                        consumer.seekToBeginning(topicPartition);
                    } else {
                        LOGGER.info("Seeking subscriber {} for feed topic {} with partition {} to offset {}", subscription.getId(), feed.getName(), 0, startArticleId);
                        consumer.seek(topicPartition, startArticleId);
                    }
                }
            });

            return getArticles(subscription, count, feed, consumer);
        }
    }

    private List<Article> getArticles(Subscription subscription,
                                      int count,
                                      Feed feed,
                                      KafkaConsumer<String, String> consumer) {
        int i = 0;
        long elapsedTime = 0;
        final List<Article> articles = Lists.newLinkedList();

        LOGGER.info("Getting articles for subscription {} and feed with id {} and name {}",
                subscription.getId(), feed.getId(), feed.getName());
        final long startTime = System.currentTimeMillis();
        Optional<ConsumerRecord<String, String>> lastConsumedRecord = Optional.empty();
        while (i < count && elapsedTime < CONSUMER_FETCH_RECORDS_MAX_TIME_MS) {
            final ConsumerRecords<String, String> records = consumer.poll(CONSUMER_POLL_MAX_TIME_MS);
            final Iterable<ConsumerRecord<String, String>> recordsIterable = records.records(feed.getName());
            for (ConsumerRecord<String, String> record : recordsIterable) {
                try {
                    final Article article = Article.fromJson(record.value());
                    article.setId(record.offset());
                    articles.add(article);
                    i++;
                    lastConsumedRecord = Optional.of(record);
                    //markReadTill(subscription, record.offset());
                } catch (IOException ex) {
                    throw new FeedReaderException("Unable to deserialize the consumer record: " + ex.getMessage());
                }
            }
            elapsedTime = System.currentTimeMillis() - startTime;
        }

        autoMarkReadTillLastRecord(subscription, lastConsumedRecord);
        return articles;
    }

    private void autoMarkReadTillLastRecord(Subscription subscription, Optional<ConsumerRecord<String, String>> lastConsumedRecord) {
        if (autoMarkRead && lastConsumedRecord.isPresent()) {
            markReadTill(subscription, lastConsumedRecord.get().offset() + 1);
        }
    }

    /**
     * This method should be thread-safe
     * Concurrent access to this should work fine
     *
     * @param subscriptions           to get articles for
     * @param maxCountPerSubscription the max. no of articles to get
     * @return the map of subscription to articles
     */
    @Override
    public Map<Subscription, List<Article>> getArticles(Set<Subscription> subscriptions, int maxCountPerSubscription) {
        final List<String> feedNames = subscriptions.stream().map(subscription -> {
            final Feed feed = getFeed(subscription);
            return feed.getName();
        }).collect(Collectors.toList());

        consumerProperties.put("group.id", "subscription-" + UUID.randomUUID().toString());
        try (KafkaConsumer<String, String> consumer = new KafkaConsumer<>(consumerProperties)) {
            consumer.subscribe(feedNames, new ConsumerRebalanceListener() {
                @Override
                public void onPartitionsRevoked(Collection<TopicPartition> partitions) {
                }

                @Override
                public void onPartitionsAssigned(Collection<TopicPartition> partitions) {
                    subscriptions.forEach((Subscription subscription) -> {
                        final Feed feed = getFeed(subscription);
                        final TopicPartition topicPartition = new TopicPartition(feed.getName(), 0);
                        long lastReadArticleId = subscription.getLastReadArticleId();
                        if (lastReadArticleId == 0) {
                            LOGGER.info("Seeking subscriber {} for feed topic {} with partition {} to the beginning",
                                    subscription.getId(), feed.getName(), 0);
                            consumer.seekToBeginning(topicPartition);
                        } else {
                            LOGGER.info("Seeking subscriber {} for feed topic {} with partition {} to offset {}",
                                    subscription.getId(), feed.getName(), 0, lastReadArticleId);
                            consumer.seek(topicPartition, lastReadArticleId);
                        }
                    });
                }
            });

            /**
             * Note: This code is almost the same as the above method but didn't generalize
             * as there are different type of variables across these methods
             *
             * Also the looping is sequential to simplify the records
             */
            final ConsumerRecords<String, String> records = consumer.poll(CONSUMER_POLL_MAX_TIME_MS);
            final Map<Subscription, List<Article>> subArticlesMap = new ConcurrentHashMap<>();

            //TODO: caution! and change this later: parallelStream() uses a common Fork-Join threadpool which can backfire if the threads are waiting on I/O for a long time.
            subscriptions.parallelStream().forEach((Subscription subscription) -> {
                //for (Subscription subscription : subscriptions) {
                int i = 0;
                final Feed feed = getFeed(subscription);
                final List<Article> articles = Lists.newLinkedList();
                Optional<ConsumerRecord<String, String>> lastConsumedRecord = Optional.empty();
                for (ConsumerRecord<String, String> record : records.records(feed.getName())) {
                    try {
                        final Article article = Article.fromJson(record.value());
                        article.setId(record.offset());
                        articles.add(article);
                        lastConsumedRecord = Optional.of(record);

                        i++;
                        if (i >= maxCountPerSubscription) {
                            break;
                        }
                    } catch (IOException ex) {
                        throw new FeedReaderException("Unable to deserialize the consumer record: " + ex.getMessage());
                    }
                }
                subArticlesMap.put(subscription, articles);
                autoMarkReadTillLastRecord(subscription, lastConsumedRecord);
            });

            return subArticlesMap;
        }
    }

    private Feed getFeed(Subscription subscription) {
        final Optional<Feed> feedOptional = feedService.getFeed(subscription.getFeedId());
        EntityValidation.checkFeedExistence(subscription.getFeedId(), feedOptional);
        return feedOptional.get();
    }

    @Override
    public void deleteSubscription(Subscription subscription) {
        super.deleteSubscription(subscription);

        final Feed feed = getFeed(subscription);
        final KafkaConsumer<String, String> consumer = new KafkaConsumer<>(consumerProperties);
        consumer.subscribe(Collections.singletonList(feed.getName()));
        consumer.unsubscribe();
    }
}
