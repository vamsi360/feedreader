package com.feedreader.exceptions;

/**
 * Created by vamsi on 18/03/16.
 */
public class DuplicateEntityException extends FeedReaderException {
    public DuplicateEntityException() {
        super();
    }

    public DuplicateEntityException(String message) {
        super(message);
    }
}
