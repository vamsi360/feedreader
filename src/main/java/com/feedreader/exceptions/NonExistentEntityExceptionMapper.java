package com.feedreader.exceptions;

import io.dropwizard.jersey.errors.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by vamsi on 18/03/16.
 */
public class NonExistentEntityExceptionMapper implements ExceptionMapper<NonExistentEntityException> {

    @Override
    public Response toResponse(NonExistentEntityException exception) {
        final Response.Status status = Response.Status.NOT_FOUND;
        return Response.status(status).entity(new ErrorMessage(status.getStatusCode(), exception.getMessage())).build();
    }
}
