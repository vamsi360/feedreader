package com.feedreader.exceptions;

import io.dropwizard.jersey.errors.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by vamsi on 18/03/16.
 */
public class InvalidEntityExceptionMapper implements ExceptionMapper<InvalidEntityException> {

    @Override
    public Response toResponse(InvalidEntityException exception) {
        final Response.Status status = Response.Status.BAD_REQUEST;
        return Response.status(status).entity(new ErrorMessage(status.getStatusCode(), exception.getMessage())).build();
    }
}
