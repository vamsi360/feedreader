package com.feedreader.exceptions;

/**
 * Created by vamsi on 18/03/16.
 */
public class NonExistentEntityException extends RuntimeException {
    public NonExistentEntityException() {
        super();
    }

    public NonExistentEntityException(String message) {
        super(message);
    }
}
