package com.feedreader.exceptions;

/**
 * Created by vamsi on 19/03/16.
 */
public class ArticlePublishingException extends FeedReaderException {
    public ArticlePublishingException() {
        super();
    }

    public ArticlePublishingException(String message) {
        super(message);
    }
}
