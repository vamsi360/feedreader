package com.feedreader.exceptions;

/**
 * Created by vamsi on 18/03/16.
 */
public class FeedReaderException extends RuntimeException {
    public FeedReaderException() {
        super();
    }

    public FeedReaderException(String message) {
        super(message);
    }
}
