package com.feedreader.exceptions;

/**
 * Created by vamsi on 18/03/16.
 */
public class InvalidEntityException extends FeedReaderException {
    public InvalidEntityException() {
        super();
    }

    public InvalidEntityException(String message) {
        super(message);
    }
}
