package com.feedreader.exceptions;

import io.dropwizard.jersey.errors.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by vamsi on 18/03/16.
 */
public class DuplicateEntityExceptionMapper implements ExceptionMapper<DuplicateEntityException> {

    @Override
    public Response toResponse(DuplicateEntityException exception) {
        final Response.Status status = Response.Status.CONFLICT;
        return Response.status(status).entity(new ErrorMessage(status.getStatusCode(), exception.getMessage())).build();
    }
}