package com.feedreader.exceptions;

import io.dropwizard.jersey.errors.ErrorMessage;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Created by vamsi on 19/03/16.
 */
public class AppThrowableMapper implements ExceptionMapper<Throwable> {
    @Override
    public Response toResponse(Throwable exception) {
        final Response.Status status = Response.Status.INTERNAL_SERVER_ERROR;
        return Response.status(status).entity(new ErrorMessage(status.getStatusCode(), exception.getMessage())).build();
    }
}
