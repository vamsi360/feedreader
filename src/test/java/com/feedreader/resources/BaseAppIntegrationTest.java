package com.feedreader.resources;

import com.feedreader.FeedReaderApplication;
import com.feedreader.FeedReaderConfiguration;
import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.entities.Subscription;
import com.feedreader.entities.User;
import com.feedreader.exceptions.FeedReaderException;
import com.feedreader.utils.BaseTestHelper;
import com.google.common.collect.Lists;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.testing.ResourceHelpers;
import org.apache.zookeeper.server.quorum.QuorumPeerConfig;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/**
 * Created by vamsi on 18/03/16.
 */
public abstract class BaseAppIntegrationTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseAppIntegrationTest.class);

    //the below code is a hack to read the config before the app starts
    //the situation here is: we need to start Kafka/Zk before the app and only when app loads do we need config
    //this is a bit of hacking into dropwizard code - FeedAppRule stuff is a wrapper
    private static String primaryStore = "mysql"; // default mysql - will be change

    static {
        try {
            File file = new File(BaseTestHelper.getFilePath(ClassLoader.getSystemClassLoader(), "test-config.yml"));
            Yaml yaml = new Yaml();
            final Map<String, Object> map = (Map<String, Object>) yaml.load(new FileInputStream(file));
            primaryStore = String.valueOf(map.get("primaryStore"));
        } catch (FileNotFoundException e) {
            LOGGER.error("Error in loading file", e);
        }
    }

    @ClassRule
    public static final FeedAppRule<FeedReaderConfiguration> RULE =
            new FeedAppRule<>(primaryStore, FeedReaderApplication.class, ResourceHelpers.resourceFilePath("test-config.yml"));

    public static Client client;
    final List<String> sqlStatements = Lists.newLinkedList();

    @BeforeClass
    public static void baseBeforeClass() throws IOException, QuorumPeerConfig.ConfigException, InterruptedException {
        //client = new JerseyClientBuilder(RULE.getEnvironment()).build("test-client");
        client = ClientBuilder.newClient();
    }

    @AfterClass
    public static void baseAfterClass() {
        client.close();
    }

    @Before
    public void baseBeforeMethod() throws SQLException {
        beforeTest();
        resetDB(sqlStatements);
    }

    @Before
    public void baseAfterMethod() throws SQLException {
        afterTest();
    }

    protected abstract void beforeTest();

    protected abstract void afterTest();

    public void resetDB(List<String> sqlStatements) throws SQLException {
        final DBIFactory dbiFactory = new DBIFactory();
        final DBI jdbi = dbiFactory.build(RULE.getEnvironment(), RULE.getConfiguration().getDatabase(), "mysql");

        final Connection connection = jdbi.open().getConnection();

        sqlStatements.forEach((String sqlStatement) -> {
            try {
                executeDML(connection, sqlStatement);
            } catch (SQLException e) {
                throw new FeedReaderException(e.getMessage());
            }
        });

        if (connection != null && !connection.isClosed()) {
            connection.close();
        }
    }

    public void executeDML(Connection connection, String sql) throws SQLException {
        final PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.executeUpdate();
    }


    public void createFeed(String feedJson) {
        final Response response = client.target(String.format("http://localhost:%s/feeds", RULE.getLocalPort()))
                .request().post(Entity.json(feedJson));
        assertEquals(201, response.getStatus());
    }

    public void createUser(String userJson) {
        final Response response = client.target(String.format("http://localhost:%s/users", RULE.getLocalPort()))
                .request().post(Entity.json(userJson));
        assertEquals(201, response.getStatus());
    }

    public void createSubscription(String subscriptionJson) {
        final Response response = client.target(String.format("http://localhost:%s/subscriptions", RULE.getLocalPort()))
                .request().post(Entity.json(subscriptionJson));
        assertEquals(201, response.getStatus());
    }

    public Set<Feed> getFeeds() {
        final Response response = client.target(String.format("http://localhost:%s/feeds", RULE.getLocalPort()))
                .request().get();
        assertEquals(200, response.getStatus());
        return response.readEntity(new GenericType<Set<Feed>>() {
        });
    }

    public Set<User> getUsers() {
        final Response response = client.target(String.format("http://localhost:%s/users", RULE.getLocalPort()))
                .request().get();
        assertEquals(200, response.getStatus());
        return response.readEntity(new GenericType<Set<User>>() {
        });
    }

    public Set<Subscription> getSubscriptions() {
        final Response response = client.target(String.format("http://localhost:%s/subscriptions", RULE.getLocalPort()))
                .request().get();
        assertEquals(200, response.getStatus());
        return response.readEntity(new GenericType<Set<Subscription>>() {
        });
    }

    public Article postArticle(Feed feed, String articleJson) {
        final Response response = client.target(String.format("http://localhost:%s/feeds/%d/articles", RULE.getLocalPort(), feed.getId()))
                .request().post(Entity.json(articleJson));
        assertEquals(200, response.getStatus());
        return response.readEntity(Article.class);
    }

    public List<Article> getArticles(Subscription subscription) {
        final Response response = client.target(String.format("http://localhost:%s/subscriptions/%d/articles", RULE.getLocalPort(), subscription.getId()))
                .request().get();
        assertEquals(200, response.getStatus());
        return response.readEntity(new GenericType<List<Article>>() {
        });
    }

    public Map<Long, List<Article>> getArticles(User user, int articleCount) {
        final Response response = client.target(String.format("http://localhost:%s/users/%s/articles?count=%d", RULE.getLocalPort(), user.getId(), articleCount))
                .request().get();
        assertEquals(200, response.getStatus());
        return response.readEntity(new GenericType<Map<Long, List<Article>>>() {
        });
    }
}
