package com.feedreader.resources;

import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import java.io.IOException;
import java.util.Set;

/**
 * Created by vamsi on 18/03/16.
 */
public class FeedResourceIntegrationTest extends BaseAppIntegrationTest {

    private void cleanDB() {
        sqlStatements.clear();
        sqlStatements.add("truncate users");
        sqlStatements.add("truncate feeds");
        sqlStatements.add("truncate articles");
        sqlStatements.add("truncate subscriptions");
    }

    @Override
    public void beforeTest() {
        cleanDB();
    }

    @Override
    protected void afterTest() {
        cleanDB();
    }

    @Test
    public void shouldCreateNewFeed() throws IOException {
        final Feed feed = Feed.builder().name(RandomStringUtils.randomAlphanumeric(5)).build();
        createFeed(feed.toJson());
    }

    @Test
    public void shouldPublishArticlesToFeed() throws IOException {
        final Feed feed = Feed.builder().name(RandomStringUtils.randomAlphanumeric(5)).build();
        createFeed(feed.toJson());

        final Set<Feed> feeds = getFeeds();
        final Feed createdFeed = feeds.iterator().next();

        final String articleId = RandomStringUtils.randomAlphabetic(5);
        final Article article = Article.builder().articleId(articleId).content(articleId).heading(articleId).build();
        postArticle(createdFeed, article.toJson());
    }

}