package com.feedreader.resources;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.feedreader.entities.Article;
import com.feedreader.entities.Feed;
import com.feedreader.entities.Subscription;
import com.feedreader.entities.User;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;

import java.util.*;
import java.util.concurrent.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by vamsi on 20/03/16.
 */
public class MainIntegrationTest extends BaseAppIntegrationTest {

    private void cleanDB() {
        sqlStatements.clear();
        sqlStatements.add("truncate users");
        sqlStatements.add("truncate feeds");
        sqlStatements.add("truncate articles");
        sqlStatements.add("truncate subscriptions");
    }

    @Override
    public void beforeTest() {
        cleanDB();
    }

    @Override
    protected void afterTest() {
        cleanDB();
    }

    /**
     * This test is a full integration test
     * 1. Create two feeds and a single user
     * 2. Create two subscriptions for the same user covering the two feeds
     * 3. Now post articles to both the feeds and read articles from the subscriptions
     * 4. The posted articles and subscribed articles should match
     * <p>
     * assumes that the autoMarkRead is false
     *
     * @throws JsonProcessingException
     */
    @Test
    public void shouldCreateFeedsAndSubscribe_receiveAllPublishedArticles() throws JsonProcessingException {
        final String feedName1 = RandomStringUtils.randomAlphanumeric(5);
        final String feedName2 = RandomStringUtils.randomAlphanumeric(5);
        final String userName = RandomStringUtils.randomAlphanumeric(5);
        final String subName = RandomStringUtils.randomAlphanumeric(5);
        final String subName2 = RandomStringUtils.randomAlphanumeric(5);
        final String articleId1 = RandomStringUtils.randomAlphanumeric(5);
        final String articleId2 = RandomStringUtils.randomAlphanumeric(5);

        final Feed feed1 = Feed.builder().name(feedName1).description(feedName1).build();
        createFeed(feed1.toJson());
        final Feed feed2 = Feed.builder().name(feedName2).description(feedName2).build();
        createFeed(feed2.toJson());
        final Set<Feed> createdFeeds = getFeeds();
        final Iterator<Feed> feedIterator = createdFeeds.iterator();
        final Feed createdFeed1 = feedIterator.next();
        final Feed createdFeed2 = feedIterator.next();

        final User user = User.builder().name(userName).email(userName + "@example.com").build();
        createUser(user.toJson());
        final User createdUser = getUsers().iterator().next();

        final Subscription subscription1 = Subscription.builder()
                .name(subName).feedId(createdFeed1.getId()).userId(createdUser.getId()).build();
        final Subscription subscription2 = Subscription.builder()
                .name(subName2).feedId(createdFeed2.getId()).userId(createdUser.getId()).build();

        createSubscription(subscription1.toJson());
        createSubscription(subscription2.toJson());

        final List<Subscription> subscriptionList = Lists.newArrayList(getSubscriptions());
        Collections.sort(subscriptionList, (sub1, sub2) -> (int) (sub1.getId() - sub2.getId()));

        final Iterator<Subscription> subIterator = subscriptionList.iterator();
        final Subscription createdSubscription1 = subIterator.next();
        final Subscription createdSubscription2 = subIterator.next();

        final int noOfArticles = 5;
        final List<Article> postedArticlesToFeed1 = Lists.newLinkedList();
        final List<Article> postedArticlesToFeed2 = Lists.newLinkedList();
        for (int i = 0; i < noOfArticles; i++) {
            final Article article = Article.builder().articleId(articleId1).content("content1").heading("heading1").build();
            final Article postedArticleToFeed1 = postArticle(createdFeed1, article.toJson());
            postedArticlesToFeed1.add(postedArticleToFeed1);

            final Article article2 = Article.builder().articleId(articleId2).content("content2").heading("heading2").build();
            final Article postedArticleToFeed2 = postArticle(createdFeed2, article2.toJson());
            postedArticlesToFeed2.add(postedArticleToFeed2);
        }

        /**
         * get the articles published for each subscription individually
         */
        final List<Article> subscribedArticles1 = getArticles(createdSubscription1);
        final List<Article> subscribedArticles2 = getArticles(createdSubscription2);
        assertEquals(noOfArticles, subscribedArticles1.size());
        assertEquals(noOfArticles, subscribedArticles2.size());

        assertEquals(postedArticlesToFeed1, subscribedArticles1);
        assertEquals(postedArticlesToFeed2, subscribedArticles2);

        /**
         * now get the same articles using the bulk api to get all subscribed feeds for a user
         */
        final Map<Long, List<Article>> allFeedArticles = getArticles(createdUser, 10);
        final List<Article> articlesForFeed1 = allFeedArticles.get(createdFeed1.getId());
        final List<Article> articlesForFeed2 = allFeedArticles.get(createdFeed2.getId());
        assertEquals(postedArticlesToFeed1, articlesForFeed1);
        assertEquals(postedArticlesToFeed2, articlesForFeed2);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void shouldCreateFeedsAndSubscribe_publishArticlesConcurrentlyAndReadShouldReadAllPublishedMessages()
            throws JsonProcessingException, InterruptedException, ExecutionException {
        final String feedName1 = RandomStringUtils.randomAlphanumeric(5);
        final String feedName2 = RandomStringUtils.randomAlphanumeric(5);
        final String userName = RandomStringUtils.randomAlphanumeric(5);
        final String subName = RandomStringUtils.randomAlphanumeric(5);
        final String subName2 = RandomStringUtils.randomAlphanumeric(5);
        final String articleId1 = RandomStringUtils.randomAlphanumeric(5);
        final String articleId2 = RandomStringUtils.randomAlphanumeric(5);

        final Feed feed1 = Feed.builder().name(feedName1).description(feedName1).build();
        createFeed(feed1.toJson());
        final Feed feed2 = Feed.builder().name(feedName2).description(feedName2).build();
        createFeed(feed2.toJson());
        final Set<Feed> createdFeeds = getFeeds();
        final Iterator<Feed> feedIterator = createdFeeds.iterator();
        final Feed createdFeed1 = feedIterator.next();
        final Feed createdFeed2 = feedIterator.next();

        final User user = User.builder().name(userName).email(userName + "@example.com").build();
        createUser(user.toJson());
        final User createdUser = getUsers().iterator().next();

        final Subscription subscription1 = Subscription.builder()
                .name(subName).feedId(createdFeed1.getId()).userId(createdUser.getId()).build();
        final Subscription subscription2 = Subscription.builder()
                .name(subName2).feedId(createdFeed2.getId()).userId(createdUser.getId()).build();

        createSubscription(subscription1.toJson());
        createSubscription(subscription2.toJson());

        final int noOfArticles = 5;
        final int noOfThreads = 10;
        final ExecutorService executorService = Executors.newFixedThreadPool(noOfThreads);

        final Collection<Callable<Object>> callableCollection = Lists.newLinkedList();
        for (int i = 0; i < noOfThreads; i++) {
            final TestArticlePublisher<Object> publisher = new TestArticlePublisher(createdFeed1,
                    createdFeed2,
                    articleId1, articleId2, noOfArticles);
            callableCollection.add(publisher);
        }

        final List<Future<Object>> futures = Lists.newLinkedList();
        futures.addAll(executorService.invokeAll(callableCollection));

        final List<Future<?>> failedFutures = Lists.newLinkedList();
        for (Future<Object> future : futures) {
            if (!Objects.isNull(future.get())) { // null => succeeded
                failedFutures.add(future);
            }
        }

        //there shouldn't be any failure futures
        assertEquals(0, failedFutures.size());

        /**
         * now get the same articles using the bulk api to get all subscribed feeds for a user
         */
        final Map<Long, List<Article>> allFeedArticles = getArticles(createdUser, noOfArticles * noOfThreads * 2);
        final List<Article> articlesForFeed1 = allFeedArticles.get(createdFeed1.getId());
        final List<Article> articlesForFeed2 = allFeedArticles.get(createdFeed2.getId());
        assertEquals(noOfArticles * noOfThreads, articlesForFeed1.size());
        assertEquals(noOfArticles * noOfThreads, articlesForFeed2.size());
    }

    /**
     * call() methods returns null. Careful!
     *
     * @param <T>
     */
    @AllArgsConstructor
    class TestArticlePublisher<T> implements Callable {
        private final Feed createdFeed1;
        private final Feed createdFeed2;
        private final String articleId1;
        private final String articleId2;
        private int maxNoOfArticlesPerFeed;

        @Override
        public T call() {
            try {
                for (int i = 0; i < maxNoOfArticlesPerFeed; i++) {
                    final Article article = Article.builder().articleId(articleId1).content("content1").heading("heading1").build();
                    final Article postedArticleToFeed1 = postArticle(createdFeed1, article.toJson());

                    final Article article2 = Article.builder().articleId(articleId2).content("content2").heading("heading2").build();
                    final Article postedArticleToFeed2 = postArticle(createdFeed2, article2.toJson());
                }
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
