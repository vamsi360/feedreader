package com.feedreader.resources;

import com.feedreader.FeedReaderConfiguration;
import com.feedreader.utils.BaseTestHelper;
import io.dropwizard.Application;
import io.dropwizard.testing.ConfigOverride;
import io.dropwizard.testing.DropwizardTestSupport;
import io.dropwizard.testing.junit.DropwizardAppRule;
import kafka.server.KafkaConfig;
import kafka.server.KafkaServerStartable;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.zookeeper.server.ServerConfig;
import org.apache.zookeeper.server.ZooKeeperServerMain;
import org.apache.zookeeper.server.quorum.QuorumPeerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by vamsi on 20/03/16.
 */
public class FeedAppRule<F> extends DropwizardAppRule<FeedReaderConfiguration> {
    private static final Logger LOGGER = LoggerFactory.getLogger(FeedAppRule.class);

    private final String primaryStore;

    public FeedAppRule(String primaryStore, Class<? extends Application<FeedReaderConfiguration>> applicationClass, @Nullable String configPath, ConfigOverride... configOverrides) {
        super(applicationClass, configPath, configOverrides);
        this.primaryStore = primaryStore;
    }

    public FeedAppRule(DropwizardTestSupport<FeedReaderConfiguration> testSupport, String primaryStore) {
        super(testSupport);
        this.primaryStore = primaryStore;
    }

    private KafkaServerStartable kafkaServer;
    private Thread zkServer;

    @Override
    protected void before() {
        if ("kafka".equals(primaryStore)) {
            LOGGER.info("Starting Kafka and zk as primary store is Kafka");
            final Properties zkProperties = new Properties();
            final String zkPropsFileLoc = BaseTestHelper.getFilePath(ClassLoader.getSystemClassLoader(), "zk.test.properties");
            try {
                zkProperties.load(new FileInputStream(new File(zkPropsFileLoc)));
            } catch (IOException e) {
                e.printStackTrace();
            }

            final String kafkaPropsFileLoc = BaseTestHelper.getFilePath(ClassLoader.getSystemClassLoader(), "kafka.test.properties");
            final Properties kafkaProperties = new Properties();
            try {
                kafkaProperties.load(new FileInputStream(new File(kafkaPropsFileLoc)));
            } catch (IOException e) {
                e.printStackTrace();
            }

            kafkaProperties.put("log.dirs", "/tmp/kafka-test-" + RandomStringUtils.randomAlphanumeric(6));
            zkProperties.put("dataDir", "/tmp/zk-data-test-" + RandomStringUtils.randomAlphanumeric(6));

            final ZooKeeperServerMain zooKeeperServer = new ZooKeeperServerMain();
            final ServerConfig zkServerConfig = new ServerConfig();
            final QuorumPeerConfig quorumPeerConfig = new QuorumPeerConfig();
            try {
                quorumPeerConfig.parseProperties(zkProperties);
            } catch (IOException | QuorumPeerConfig.ConfigException e) {
                e.printStackTrace();
            }
            zkServerConfig.readFrom(quorumPeerConfig);

            zkServer = new Thread(() -> {
                try {
                    zooKeeperServer.runFromConfig(zkServerConfig);
                } catch (IOException ex) {
                    LOGGER.error("Unable to start zookeeper", ex);
                }
            });
            zkServer.start();

            final KafkaConfig kafkaConfig = new KafkaConfig(kafkaProperties);

            LOGGER.info("Starting Kafka server");
            kafkaServer = new KafkaServerStartable(kafkaConfig);
            kafkaServer.startup();
        }

        super.before();
    }

    @Override
    protected void after() {
        super.after();

        final String primaryStore = getConfiguration().getPrimaryStore();
        if ("kafka".equals(primaryStore)) {
            LOGGER.info("Shutting down Kafka and zk as primary store is Kafka");
            kafkaServer.shutdown();
            kafkaServer.awaitShutdown();
            zkServer.stop();
        }
    }
}
