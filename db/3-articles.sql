CREATE TABLE `articles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `article_id` varchar(100) NOT NULL,
  `feed_id` bigint(20) DEFAULT NULL,
  `heading` varchar(100) NOT NULL,
  `content` text,
  `content_type` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;
